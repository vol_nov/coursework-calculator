#pragma once
#include <Windows.h>
#include <map>
#include <string>
#include "Converter.h"
#include "Calculate.h"
#include "resource.h"

class WindowHandler {
	HWND hWndEditBox;
	HWND hWndComboBox;
	bool dotEnabled;
	int prevSelectedCountingSystemIndex;
	std::map<char, HWND> numbers;
	std::map<std::string, HWND> operations;
	std::map<std::string, HWND> mainSignals;
public:
	void OnCreate(HWND hWnd);
	void OnCommand(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	void OnChar(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	void OnPaint(HWND hWnd);
};