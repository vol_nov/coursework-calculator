#pragma once

#include <type_traits>

template <typename T>
class my_collection {
	T* arr;
	int length;
public:
	my_collection();
	my_collection(T* arr, int length);

	int first_index(T elem, int start_index);
	int last_index(T elem, int start_index);
	void add_element(T elem);
	void insert_range_of_elements(T * elements, int elements_length, int index);
	void remove_element(int index);
	void remove_element(T elem);
	void remove_range_of_elements(int start, int end);

	int get_length();
	void clear();

	T*& get_data();

	T& operator[] (int i);

	int count_of(T elem);
	void set(T* elem, int length);

	~my_collection();
};
