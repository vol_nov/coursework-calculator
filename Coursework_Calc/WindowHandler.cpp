#include "WindowHandler.h"


void WindowHandler::OnCreate(HWND hWnd) {
	prevSelectedCountingSystemIndex = 1;

	int btnSize = 36;
	int btnMargin = 10;

	hWndComboBox = CreateWindow("COMBOBOX", TEXT(""),
		CBS_DROPDOWN | CBS_HASSTRINGS | WS_CHILD | WS_OVERLAPPED | WS_VISIBLE,
		10, 30, btnSize * 6 + 5 * btnMargin, 200, hWnd, NULL, GetModuleHandle(NULL),
		NULL);

	TCHAR countingSystem[3][20] =
	{
		TEXT("�������"), TEXT("���������"), TEXT("س������������")
	};

	TCHAR A[16];

	memset(&A, 0, sizeof(A));
	for (int k = 0; k < 3; k++)
	{
		strcpy_s(A, sizeof(A) / sizeof(TCHAR), (TCHAR*)countingSystem[k]);
		SendMessage(hWndComboBox, (UINT)CB_ADDSTRING, (WPARAM)k, (LPARAM)A);
	}

	SendMessage(hWndComboBox, CB_SETCURSEL, (WPARAM)1, (LPARAM)0);

	dotEnabled = true;

	hWndEditBox = CreateWindowEx(WS_EX_CLIENTEDGE,
		"EDIT", "", WS_CHILD | WS_VISIBLE | ES_MULTILINE | ES_AUTOVSCROLL | ES_READONLY | ES_AUTOHSCROLL,
		10, 60, btnSize * 6 + 5 * btnMargin, 25, hWnd, (HMENU)NULL, GetModuleHandle(NULL), NULL);


	int dx = 0, dy = 0;

	int btnStartX = 10;
	int btnStartY = 90;

	HWND btnHwnd;

	btnHwnd = CreateWindowEx(NULL, "BUTTON", "0",
		WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
		50, 220, btnSize, btnSize, hWnd, (HMENU)IDB_NUMBER0, GetModuleHandle(NULL), NULL);

	numbers.insert(std::pair<char, HWND>('0', btnHwnd));

	for (int i = 1; i < 16; i++) {
		if (i == 10)
		{
			btnStartX = btnSize * 3 + btnMargin * 4;
			dy = 0;
		}

		if (i < 10) {
			char* ch = new char[2];
			ch[0] = char(i + 48);
			ch[1] = '\0';

			btnHwnd = CreateWindowEx(NULL, "BUTTON", ch,
				WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
				btnStartX + dx, btnStartY + dy, btnSize, btnSize, hWnd, (HMENU)(IDB_NUMBER0 + i), GetModuleHandle(NULL), NULL);

			numbers.insert(std::pair<char, HWND>(char(i + 48), btnHwnd));

			delete ch;

			if (i % 3 == 0) {
				dx = 0;
				dy += btnSize + btnMargin;
			}
			else {
				dx += btnSize + btnMargin;
			}
		}
		else {
			char* ch = new char[2];
			ch[0] = char((i - 10) + 65);
			ch[1] = '\0';

			btnHwnd = CreateWindowEx(NULL, "BUTTON", ch,
				WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
				btnStartX + dx, btnStartY + dy, btnSize, btnSize, hWnd, (HMENU)(IDB_NUMBER0 + i), GetModuleHandle(NULL), NULL);

			numbers.insert(std::pair<char, HWND>(char((i - 10) + 65), btnHwnd));

			EnableWindow(btnHwnd, FALSE);


			delete ch;

			if (i % 2 != 0) {
				dx = 0;
				dy += btnSize + btnMargin;
			}
			else {
				dx += btnSize + btnMargin;
			}
		}
	}

	btnStartX += 2 * (btnSize + btnMargin);
	SetWindowPos(numbers.at('0'), HWND_TOP, btnStartX, btnStartY, btnSize, btnSize, SWP_NOSIZE);

	btnStartY += (btnSize + btnMargin);

	std::string buf;
	buf = "=";

	btnHwnd = CreateWindowEx(NULL, "BUTTON", buf.c_str(),
		WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
		btnStartX + dx, btnStartY, btnSize, btnSize, hWnd, (HMENU)IDB_SIGNALDELEQUALS, GetModuleHandle(NULL), NULL);

	mainSignals.insert(std::pair<std::string, HWND>(buf, btnHwnd));

	btnStartY += (btnSize + btnMargin);

	buf = "C";

	btnHwnd = CreateWindowEx(NULL, "BUTTON", buf.c_str(),
		WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
		btnStartX + dx, btnStartY, btnSize, btnSize, hWnd, (HMENU)IDB_SIGNALDELALL, GetModuleHandle(NULL), NULL);

	mainSignals.insert(std::pair<std::string, HWND>(buf, btnHwnd));

	btnStartY += (btnSize + btnMargin);

	buf = "<-";

	btnHwnd = CreateWindowEx(NULL, "BUTTON", buf.c_str(),
		WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
		btnStartX + dx, btnStartY, btnSize, btnSize, hWnd, (HMENU)IDB_SIGNALDELLAST, GetModuleHandle(NULL), NULL);

	mainSignals.insert(std::pair<std::string, HWND>(buf, btnHwnd));

	btnStartX = 10;
	btnStartY = 90 + 3 * (btnSize + btnMargin);

	for (int i = 0; i < 5; i++) {
		switch (i) {
		case 0:
			buf = "+";
			break;
		case 1:
			buf = "-";
			break;
		case 2:
			buf = "*";
			break;
		case 3:
			buf = "/";
			break;
		case 4:
			buf = "1/x";
			break;
		}

		btnHwnd = CreateWindowEx(NULL, "BUTTON", buf.c_str(),
			WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
			btnStartX + dx, btnStartY, btnSize, btnSize, hWnd, (HMENU)(IDB_OPERATIONPLUS + i), GetModuleHandle(NULL), NULL);

		operations.insert(std::pair<std::string, HWND>(buf, btnHwnd));
		dx += btnSize + btnMargin;
		buf.clear();
	}

	btnStartY += btnSize + btnMargin;

	dx = 0;

	buf = "(";

	btnHwnd = CreateWindowEx(NULL, "BUTTON", buf.c_str(),
		WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
		btnStartX + dx, btnStartY, btnSize * 2 + btnMargin, btnSize, hWnd, (HMENU)(IDB_STARTBRACKET), GetModuleHandle(NULL), NULL);

	operations.insert(std::pair<std::string, HWND>(buf, btnHwnd));

	buf = ")";

	dx += (btnSize + btnMargin) * 2;
	btnHwnd = CreateWindowEx(NULL, "BUTTON", buf.c_str(),
		WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
		btnStartX + dx, btnStartY, btnSize * 2 + btnMargin, btnSize, hWnd, (HMENU)IDB_ENDBRACKET, GetModuleHandle(NULL), NULL);


	operations.insert(std::pair<std::string, HWND>(buf, btnHwnd));

	buf = ".";

	dx += (btnSize + btnMargin) * 2;

	btnHwnd = CreateWindowEx(NULL, "BUTTON", buf.c_str(),
		WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
		btnStartX + dx, btnStartY, btnSize, btnSize, hWnd, (HMENU)IDB_NUMBERDOT, GetModuleHandle(NULL), NULL);

	dx += btnSize + btnMargin;

	operations.insert(std::pair<std::string, HWND>(buf, btnHwnd));

	buf = "+/-";

	btnHwnd = CreateWindowEx(NULL, "BUTTON", buf.c_str(),
		WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
		btnStartX + dx, btnStartY, btnSize, btnSize, hWnd, (HMENU)IDB_SIGNALCHANGESIGN, GetModuleHandle(NULL), NULL);


	operations.insert(std::pair<std::string, HWND>(buf, btnHwnd));
}

void WindowHandler::OnCommand(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	int wmId = LOWORD(wParam);

	if (HIWORD(wParam) == CBN_SELCHANGE)
	{
		int itemIndex = SendMessage((HWND)lParam, (UINT)CB_GETCURSEL,
			(WPARAM)0, (LPARAM)0);

		char* convertedExp = new char[255];

		int length = GetWindowTextLength(hWndEditBox) + 1;
		char* oldText = new char[length];

		GetWindowText(hWndEditBox, oldText, length);

		if (itemIndex == prevSelectedCountingSystemIndex)
			return;

		if (itemIndex == 0) {
			for (int i = 2; i < 10; i++) {
				EnableWindow(numbers[((char)(i + 48))], FALSE);
			}
			for (int i = 0; i < 6; i++) {
				EnableWindow(numbers[((char)(i + 65))], FALSE);
			}

			if (prevSelectedCountingSystemIndex == 1) {
				convertedExp = convert_from_sys_to_sys(oldText, 10, 2);
			}
			else if (prevSelectedCountingSystemIndex == 2) {
				convertedExp = convert_from_sys_to_sys(oldText, 16, 2);
			}
			else {

			}
		}
		else if (itemIndex == 1) {
			for (int i = 2; i < 10; i++) {
				EnableWindow(numbers[((char)(i + 48))], TRUE);
			}
			for (int i = 0; i < 6; i++) {
				EnableWindow(numbers[((char)(i + 65))], FALSE);
			}

			if (prevSelectedCountingSystemIndex == 0) {
				convertedExp = convert_from_sys_to_sys(oldText, 2, 10);
			}
			else if (prevSelectedCountingSystemIndex == 2) {
				convertedExp = convert_from_sys_to_sys(oldText, 16, 10);
			}
			else {

			}
		}
		else {
			for (auto it = numbers.begin(); it != numbers.end(); it++) {
				EnableWindow((*it).second, TRUE);
			}

			if (prevSelectedCountingSystemIndex == 0) {
				convertedExp = convert_from_sys_to_sys(oldText, 2, 16);
			}
			else if (prevSelectedCountingSystemIndex == 1) {
				convertedExp = convert_from_sys_to_sys(oldText, 10, 16);
			}
			else {

			}

		}

		if (convertedExp != nullptr)
		{
			SetWindowText(hWndEditBox, convertedExp);
		}


		InvalidateRect(hWnd, NULL, FALSE);
		prevSelectedCountingSystemIndex = itemIndex;
		SetFocus(hWnd);
	}
	char* buffer;

	if (wmId >= IDB_NUMBER0 && wmId <= IDB_NUMBERF || wmId >= IDB_STARTBRACKET && wmId <= IDB_OPERATIONDIV) {
		buffer = new char[4];
		LoadString(GetModuleHandle(NULL), wmId, buffer, 4);

		int length = GetWindowTextLength(hWndEditBox) + strlen(buffer) + 1;
		char* newText = new char[length];

		GetWindowText(hWndEditBox, newText, length);

		strcat(newText, buffer);
		SetWindowText(hWndEditBox, newText);

		if (wmId >= IDB_STARTBRACKET && !dotEnabled)
		{
			dotEnabled = true;
			EnableWindow(operations["."], TRUE);
		}
	}
	else if (wmId == IDB_NUMBERDOT) {
		buffer = new char[4];
		LoadString(GetModuleHandle(NULL), wmId, buffer, 4);

		int length = GetWindowTextLength(hWndEditBox) + strlen(buffer) + 1;
		char* newText = new char[length];

		GetWindowText(hWndEditBox, newText, length);

		strcat(newText, buffer);
		SetWindowText(hWndEditBox, newText);

		EnableWindow(operations["."], FALSE);
		dotEnabled = false;
	}
	else if (wmId == IDB_SIGNALDELALL) {
		buffer = "";
		SetWindowText(hWndEditBox, buffer);
	}
	else if (wmId == IDB_SIGNALDELLAST) {
		int length = GetWindowTextLength(hWndEditBox) + 1;
		if (length > 1) {
			if (length == 2) {
				SetWindowText(hWndEditBox, "");
				dotEnabled = true;
				EnableWindow(operations["."], TRUE);
			}
			else {
				buffer = new char[length];

				GetWindowText(hWndEditBox, buffer, length);

				char* newText = new char[length - 1];
				for (int i = 0; i < length - 1; i++) {
					newText[i] = buffer[i];
				}
				newText[length - 2] = '\0';

				SetWindowText(hWndEditBox, newText);
			}
		}
	}
	else if (wmId == IDB_SIGNALDELEQUALS) {
		dotEnabled = true;
		EnableWindow(operations["."], dotEnabled);
		int length = GetWindowTextLength(hWndEditBox) + 1;
		char* buffer = new char[length];
		GetWindowText(hWndEditBox, buffer, length);

		int itemIndex = SendMessage(hWndComboBox, (UINT)CB_GETCURSEL,
			(WPARAM)0, (LPARAM)0);

		if (prevSelectedCountingSystemIndex == 0) {
			buffer = convert_from_sys_to_sys(buffer, 2, 10);
		}
		if (prevSelectedCountingSystemIndex == 2)
		{
			buffer = convert_from_sys_to_sys(buffer, 16, 10);
		}

		double res = calculate(buffer);
		char* resBuf = new char[8];
		sprintf(resBuf, "%lf", res);
		if (itemIndex == 0) {
			resBuf = convert_from_sys_to_sys(resBuf, 10, 2);
		}
		if (itemIndex == 2)
		{
			resBuf = convert_from_sys_to_sys(resBuf, 10, 16);
		}
		SetWindowText(hWndEditBox, resBuf);
	}
	else if (wmId == IDB_OPERATIONONEONX) {
		int length = GetWindowTextLength(hWndEditBox) + 1;
		char* buffer = new char[length];
		char* newText = new char[length + 4];
		GetWindowText(hWndEditBox, buffer, length);
		strcpy(newText, "1/(");
		strcat(newText, buffer);
		strcat(newText, ")");

		SetWindowText(hWndEditBox, newText);
	}
	else if (wmId == IDB_SIGNALCHANGESIGN) {
		int length = GetWindowTextLength(hWndEditBox) + 1;
		char* buffer = new char[length];
		GetWindowText(hWndEditBox, buffer, length);

		char* oldData = new char[length];
		strcpy(oldData, buffer);

		std::string strBuff = buffer;
		if (strBuff.find("(-1)", 0) == std::string::npos) {
			char* newBuff;
			char* sub = strtok(buffer, "+-*/");
			if (strtok(NULL, "+-*/") != NULL) {
				newBuff = new char[length + 7];
				strcpy(newBuff, "(");
				strcat(newBuff, oldData);
				strcat(newBuff, ")");
				strcat(newBuff, "*(-1)");
			}
			else {
				newBuff = new char[length + 5];
				strcpy(newBuff, oldData);
				strcat(newBuff, "*(-1)");
			}
			SetWindowText(hWndEditBox, newBuff);
		}
		else {

		}
	}
}

void WindowHandler::OnPaint(HWND hWnd) {
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	TextOut(hdc, 10, 10, "������� ��������", strlen("������� ��������"));
	EndPaint(hWnd, &ps);
}

void WindowHandler::OnChar(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	char ch = (char)wParam;
	int itemIndex = SendMessage(hWndComboBox, (UINT)CB_GETCURSEL,
		(WPARAM)0, (LPARAM)0);

	if (isdigit(ch)) {
		if (itemIndex == 0 && ch - '1' <= 0) {
			SendMessage(hWnd, WM_COMMAND, IDB_NUMBER0 + (char(wParam) - '0'), NULL);
		}
		if (itemIndex == 1 || itemIndex == 2) {
			SendMessage(hWnd, WM_COMMAND, IDB_NUMBER0 + (char(wParam) - '0'), NULL);
		}
	}
	else if (ch >= 'A' && ch <= 'F' || ch >= 'a' && ch <= 'f') {
		char diff = 'A';
		if (ch >= 97)
			diff = 'a';
		if (itemIndex == 2) {
			SendMessage(hWnd, WM_COMMAND, IDB_NUMBERA + (char(wParam) - diff), NULL);
		}
	}
	else if (ch == '+') {
		SendMessage(hWnd, WM_COMMAND, IDB_OPERATIONPLUS, NULL);
	}
	else if (ch == '-') {
		SendMessage(hWnd, WM_COMMAND, IDB_OPERATIONMINUS, NULL);
	}
	else if (ch == '*') {
		SendMessage(hWnd, WM_COMMAND, IDB_OPERATIONMUL, NULL);
	}
	else if (ch == '/') {
		SendMessage(hWnd, WM_COMMAND, IDB_OPERATIONDIV, NULL);
	}
	else if (ch == '(') {
		SendMessage(hWnd, WM_COMMAND, IDB_STARTBRACKET, NULL);
	}
	else if (ch == ')') {
		SendMessage(hWnd, WM_COMMAND, IDB_ENDBRACKET, NULL);
	}
	else if (ch == '.') {
		if (dotEnabled)
			SendMessage(hWnd, WM_COMMAND, IDB_NUMBERDOT, NULL);
	}
	else if (ch == '=') {
		SendMessage(hWnd, WM_COMMAND, IDB_SIGNALDELEQUALS, NULL);
	}
	else if (ch == 8) {
		SendMessage(hWnd, WM_COMMAND, IDB_SIGNALDELLAST, NULL);
	}
	else {

	}
}