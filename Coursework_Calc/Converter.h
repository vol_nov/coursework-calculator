#pragma once

double convert_num_2_to_10(char* str);
double convert_num_16_to_10(char* str);
char* convert_num_10_to_2(double num);
char* convert_num_10_to_16(double num);
char* convert_from_sys_to_sys(char* str, int from, int to);