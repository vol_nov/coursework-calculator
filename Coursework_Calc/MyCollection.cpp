#include "MyCollection.h"
#include <iostream>
template class my_collection<char>;
template class my_collection<double>;

template <typename T>
my_collection<T>::my_collection() {
	arr = nullptr;
	length = 0;
}
template <typename T>
my_collection<T>::my_collection(T* arr, int length) {
	this->length = length;
	if (std::is_same<T, char>::value) {
		this->length = strlen((char*)arr);
		this->arr = new T[length + 1];
		this->arr[length] = '\0';
		
	}
	else {
		this->arr = new T[length];
	}
	for (int i = 0; i < length; i++) {
		this->arr[i] = arr[i];
	}
}

template <typename T>
int my_collection<T>::first_index(T elem, int start_index) {
	for (int i = start_index; i < length; i++) {
		if (elem == arr[i])
			return i;
	}
}

template <typename T>
int my_collection<T>::last_index(T elem, int start_index) {
	for (int i = length - 1; i > start_index; i--) {
		if (elem == arr[i])
			return i;
	}
}

template <typename T>
void my_collection<T>::add_element(T elem) {
	if (std::is_same<T, char>::value) {
		T * new_arr = new T[length + 2];
		for (int i = 0; i < length; i++) {
			new_arr[i] = arr[i];
		}
		new_arr[length] = elem;
		new_arr[length + 1] = '\0';
		arr = new_arr;
		length++;
	}
	else {
		T * new_arr = new T[length + 1];
		for (int i = 0; i < length; i++) {
			new_arr[i] = arr[i];
		}
		new_arr[length] = elem;
		arr = new_arr;
		length++;
	}
}

template <typename T>
void my_collection<T>::insert_range_of_elements(T * elements, int elements_length, int index) {
	if (std::is_same<T, char>::value) {
		T * new_arr = new T[length + elements_length + 1];
		for (int i = 0; i < length + elements_length + 1; i++) {
			if (i < index)
				new_arr[i] = arr[i];
			else if (i > index + elements_length - 1)
				new_arr[i] = arr[i - elements_length];
			else
				new_arr[i] = elements[i - index];
		}
		length = length + elements_length + 1;
		arr = new_arr;
		arr[length - 1] = '\0';
	}
}

template <typename T>
void my_collection<T>::remove_element(int index) {

	T * new_arr = new T[length - 1];
	for (int i = 0; i < length; i++) {
		if (i < index) {
			new_arr[i] = arr[i];
		}
		if (i > index) {
			new_arr[i - 1] = arr[i];
		}
	}
	length--;
	delete arr;
	arr = new_arr;
}

template <typename T>
void my_collection<T>::remove_element(T elem) {
	int count = count_of(elem);
	int counter = 0;
	T * new_arr = new T[length - count_of(elem) + 1];
	for (int i = 0; i < length; i++) {
		if (arr[i] == elem)
			counter++;
		else
			new_arr[i - counter] = arr[i];
	}
	length = length - (count_of( elem)) + 1;
	arr = new_arr;
	arr[length - 1] = '\0';
}

template <typename T>
void my_collection<T>::remove_range_of_elements(int start, int end) {
	T * new_arr = new T[length - (end - start)];
	int rem_counter = 0;
	for (int i = 0; i < length; i++) {
		if (i < start)
			new_arr[i] = arr[i];
		if (i > end)
			new_arr[i - (end - start) - 1] = arr[i];
	}

	length = length - (end - start) - 1;
	arr = new_arr;
	arr[length] = '\0';
}

template <typename T>
int my_collection<T>::get_length() {
	return length;
}

template <typename T>
void my_collection<T>::clear() {
	if (arr != nullptr) {
		delete[]arr;
	}

	arr = nullptr;
	length = 0;
}

template <typename T>
T*& my_collection<T>::get_data() {
	return arr;
}

template <typename T>
T& my_collection<T>::operator[] (int i) {
	return arr[i];
}

template <typename T>
int my_collection<T>::count_of(T elem) {
	int count = 0;
	for (int i = 0; i < length; i++) {
		if (arr[i] == elem)
			count++;
	}
	return count;
}

template <typename T>
void my_collection<T>::set(T* elem, int length) {
	if (elem != nullptr)
		delete[] elem;

	this->length = length;
	this->arr = new T[length];
	for (int i = 0; i < length; i++) {
		this->arr[i] = elem[i];
	}
}

template <typename T>
my_collection<T>::~my_collection() {
	if (arr != nullptr)
		delete[] arr;
	length = 0;
}