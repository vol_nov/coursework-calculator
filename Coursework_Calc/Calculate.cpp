#pragma once

#include <iostream>
#include "Calculate.h"

using namespace std;

int count_of_char(char * str, char c) {
	int count = 0;
	for (int i = 0; i < strlen(str); i++) {
		if (str[i] == c)
			count++;
	}
	return count;
}


double perform_operation_inside_br(char * str) {
	my_collection<double> nums;
	my_collection<char> operations;
	my_collection<char> temp;

	for (int i = 0; i < strlen(str); i++) {
		if (isdigit(str[i]) || str[i] == '.' || str[i] == '-') {
			if (str[i] == '-' && i > 0) {
				if (str[i - 1] == '*' || str[i - 1] == '+' || str[i - 1] == '-' || str[i - 1] == '/') {
					temp.add_element(str[i]);
					continue;
				}
			}
			temp.add_element(str[i]);
		}
		if ((!isdigit(str[i]) && str[i] != '.') || i == strlen(str) - 1)
		{
			nums.add_element(atof(temp.get_data()));
			temp.clear();

			if (str[i] != ' ' && str[i] != '.' && !isdigit(str[i])) {
				operations.add_element(str[i]);
			}
		}
	}

	while (nums.get_length() != 1) {
		while (count_of_char(operations.get_data(), '*') + count_of_char(operations.get_data(), '/') != 0) {
			int i = 0;
			for (; i < operations.get_length(); i++) {
				if (operations[i] == '*' || operations[i] == '/') {
					if (operations[i] == '*')
					{
						nums[i] *= nums[i + 1];
					}
					else {
						nums[i] /= nums[i + 1];
					}
					break;
				}
			}
			operations.remove_element(i);
			nums.remove_element(i + 1);
		}

		while (count_of_char(operations.get_data(), '+') + count_of_char(operations.get_data(), '-') != 0) {
			int i = 0;
			for (; i < operations.get_length(); i++) {
				if (operations[i] == '+' || operations[i] == '-') {
					if (operations[i] == '+')
					{
						nums[i] += nums[i + 1];
					}
					else {
						nums[i] -= nums[i + 1];
					}
					break;
				}
			}
			operations.remove_element(i);
			nums.remove_element(i + 1);
		}
	}

	return nums[0];
}

double calculate(char *& str1) {

	my_collection<char> str = my_collection<char>(str1, strlen(str1));

	int len = str.get_length();
	int count_of_st_br = count_of_char(str.get_data(), '(');
	int count_of_end_br = count_of_char(str.get_data(), ')');

	if (count_of_st_br != count_of_end_br)
		return 0;

	int count_of_spaces = count_of_char(str.get_data(), ' ');
	if (count_of_spaces > 0)
		str.remove_element(' ');

	if (count_of_st_br != 0) {
		for (int i = 0; i < count_of_st_br; i++) {
			int first_break = str.last_index('(', 0);
			int last_break = str.first_index(')', first_break);

			int substr_length = last_break - first_break - 1;
			char * substr = new char[substr_length + 1];

			for (int j = first_break + 1; j < last_break; j++) {
				substr[j - first_break - 1] = str[j];
			}

			substr[substr_length] = '\0';

			double res = atof(substr);
			if (res != -1)
			  res = perform_operation_inside_br(substr);
			str.remove_range_of_elements(first_break, last_break);

			char * res_str = new char[30];
			sprintf(res_str, "%f", res);

			len++;
			str.insert_range_of_elements(res_str, strlen(res_str), first_break);
		}
	}

	return perform_operation_inside_br(str.get_data());
}

