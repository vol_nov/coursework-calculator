//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Resource.rc
//
#define IDS_APP_TITLE					102
#define IDI_COURSEWORK                  103
#define IDC_COURSEWORK                  104

#define IDB_NUMBER0						105
#define IDB_NUMBER1						106
#define IDB_NUMBER2						107
#define IDB_NUMBER3						108
#define IDB_NUMBER4						109
#define IDB_NUMBER5						110
#define IDB_NUMBER6						111
#define IDB_NUMBER7						112
#define IDB_NUMBER8						113
#define IDB_NUMBER9						114
#define IDB_NUMBERA						115
#define IDB_NUMBERB						116
#define IDB_NUMBERC						117
#define IDB_NUMBERD						118
#define IDB_NUMBERE						119
#define IDB_NUMBERF						120
#define IDB_NUMBERDOT					121
#define IDB_STARTBRACKET				122
#define IDB_ENDBRACKET					123


#define IDB_OPERATIONPLUS				124
#define IDB_OPERATIONMINUS				125
#define IDB_OPERATIONMUL				126
#define IDB_OPERATIONDIV				127
#define IDB_OPERATIONONEONX				128

#define IDB_SIGNALDELEQUALS				129
#define IDB_SIGNALDELALL				130
#define IDB_SIGNALDELLAST				131
#define IDB_SIGNALCHANGESIGN			132


// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        103
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
