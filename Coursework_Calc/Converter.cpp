#include "Converter.h"
#include "MyCollection.h"
#include <iostream>

double convert_num_2_to_10(char* str) {
	double res = 0;

	char* dot = strchr(str, '.');

	char* buf = new char[2];
	double counter = 1;

	int i;
	if (dot == NULL)
		i = strlen(str) - 1;
	else
		i = dot - str - 1;

	for (; i >= 0; i--, counter *= 2) {
		buf[0] = str[i];
		buf[1] = '\0';
		res += atoi(buf) * counter;
	}
	if (dot != NULL) {
		counter = 0.5;
		for (int i = 1; i <= strlen(dot); i++, counter /= 2) {
			buf[0] = dot[i];
			buf[1] = '\0';
			res += atoi(buf) * counter;
		}
	}

	delete buf;

	return res;
}

double convert_num_16_to_10(char* str) {
	double res = 0;

	char* dot = strchr(str, '.');

	char* buf = new char[2];
	double counter = 1;
	int num = 0;

	int i;
	if (dot == NULL)
		i = strlen(str) - 1;
	else
		i = dot - str - 1;

	for (; i >= 0; i--, counter *= 16) {
		if (str[i] >= 65) {
			num = str[i] - 55;
		}
		else {
			buf[0] = str[i];
			buf[1] = '\0';
			num = atoi(buf);
		}
		res += num * counter;
	}

	if (dot != NULL) {
		counter = 1;
		for (int i = 0; i <= strlen(dot); i++, counter /= 16) {
			if (dot[i] >= 65) {
				num = dot[i] - 55;
			}
			else {
				buf[0] = dot[i];
				buf[1] = '\0';
				num = atoi(buf);
			}
			res += num * counter;
		}
	}
	delete buf;
	return res;
}

char* convert_num_10_to_2(double num) {
	double x, y;
	y = std::modf(num, &x);
	int maxPower = 1;
	while (num > maxPower) {
		maxPower *= 2;
	}
	maxPower /= 2;

	my_collection<char> res;
	
	int div = 0;
	for (int i = x; i >= 1; i /= 2) {
		div = i % 2;
		res.add_element(div + 48);
	}
	_strrev(res.get_data());
	if (y != 0) {
		res.add_element('.');
		int counter = 0;
		while (y != 0 && counter < 8) {
			y *= 2;

			if (y >= 1) {
				res.add_element('1');
				y = std::modf(y, &x);
			}
			else {
				res.add_element('0');
			}

			counter++;
		}
	}
	char* res_buff = new char[res.get_length() + 1];
	strcpy(res_buff, res.get_data());
	return res_buff;
}

char* convert_num_10_to_16(double num) {
	if (num < 0)
		num *= -1;

	double x, y;
	y = std::modf(num, &x);
	int maxPower = 1;
	while (num > maxPower) {
		maxPower *= 16;
	}
	maxPower /= 16;

	my_collection<char> res;
	
	int savedPowers = 0;
	char c;
	int div;
	for (int i = x; i >= 1; i /= 16) {
		div = i % 16;

		if (div >= 10) {
			c = div + 55;
		}
		else {
			c = div + 48;
		}
		res.add_element(c);
	}
	_strrev(res.get_data());

	if (y != 0) {
		res.add_element('.');
		int counter = 0;
		while (y != 0 && counter < 8) {
			y *= 16;


			if (y >= 1) {
				y = std::modf(y, &x);
				if (x >= 10) {
					c = x + 55;
				}
				else {
					c = x + 48;
				}
				res.add_element(c);
			}
			else {
				res.add_element('0');
			}

			counter++;
		}
	}
	char* res_buff = new char[res.get_length() + 1];
	strcpy(res_buff, res.get_data());
	return res_buff;
}

char* convert_from_sys_to_sys(char* str, int from, int to) {
	my_collection<char> res_str = my_collection<char>("", 1);
	my_collection<char> temp_num;

	bool reading_num = false;

	bool sign = false;
	if (str[0] == '-') {
		sign = true;
	}

	for (int i = sign; i <= strlen(str); i++) {
		if (isdigit(str[i]) ||
			(str[i] >= 65 && str[i] <= 70) ||
			str[i] == '.') {
			if (i > 1) {
				if (str[i-2] == '(' && str[i - 1] == '-' && !sign) {
					res_str.add_element('1');
					continue;
				}
			}

			reading_num = true;

			temp_num.add_element(str[i]);
		}
		else {
			if (reading_num) {
				reading_num = false;

				char* res_num = nullptr;

				switch (from) {
				case 2:
					if (to == 10) {
						double num = convert_num_2_to_10(temp_num.get_data());
						res_num = new char[20];
						sprintf(res_num, "%lf", num);
					}
					else if (to == 16) {
						double num = convert_num_2_to_10(temp_num.get_data());
						res_num = convert_num_10_to_16(num);
					}
					else {

					}
					break;
				case 10:
					if (to == 2) {
						double num = atof(temp_num.get_data());
						
						res_num = convert_num_10_to_2(num);
					}
					else if (to == 16) {
						double num = atof(temp_num.get_data());
						res_num = convert_num_10_to_16(num);
					}
					else {

					}
					break;
				case 16:
					if (to == 2) {
						double num = convert_num_16_to_10(temp_num.get_data());
						res_num = convert_num_10_to_2(num);
					}
					else if (to == 10) {
						double num = convert_num_16_to_10(temp_num.get_data());
						res_num = new char[20];
						sprintf(res_num, "%lf", num);
					}
					else {

					}
					break;
				}

				char* buffer = new char[strlen(res_str.get_data()) + strlen(res_num) + 2];
				strcpy(buffer, res_str.get_data());
				
				if (sign)
					strcat(buffer, "-");
				
				strcat(buffer, res_num);

				res_str.clear();
				
				for (int i = 0; i < strlen(buffer); i++) {
					res_str.add_element(buffer[i]);
				}

				temp_num.clear();

				sign = false;
			}
			res_str.add_element(str[i]);
		}
	}
	char* res_buff = new char[res_str.get_length() + 1];
	strcpy(res_buff, res_str.get_data());
	return res_buff;
}